'use strict';

angular
.module('ancestryApp', [
  'ngAnimate',
  'ngAria',
  'ngMessages',
  'ngSanitize',
  'ngTouch',
  'ngDialog'
])

.run(['$rootScope', function($scope){

	$scope.breadcrumbs = [
		{ title: 'Home', state: 'home' },
		{ title: 'Search', state: 'search' },
		{ title: 'U.S. Military Collection', shortTitle: 'US Military Collection' },
	];

	$scope.currentPage = $scope.breadcrumbs[$scope.breadcrumbs.length-1];

	$scope.navTo = function(crumb) {
		if(crumb.state) {
			console.log('Should go to the', crumb.state, 'state');
		}
	};
}]);
