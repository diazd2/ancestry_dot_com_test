'use strict';

angular
.module('ancestryApp')
.controller('SubscribeCtrl',
	[
		'$scope',
		'$timeout',
		function ($scope, $timeout) {
			$scope.subscribe = function(form){
				$scope.showErrors = false;
				if(!form.$valid) {
					$scope.showErrors = true;
				} else {
					// subscription request here...
					// then:
					$scope.done = true;
					$timeout(function(){
						$scope.closeThisDialog();
					}, 2000);
				}
			};

		}
	]
);
