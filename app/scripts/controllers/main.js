'use strict';

angular
.module('ancestryApp')
.controller('MainCtrl',
	[ '$scope',
		'ngDialog',
		function ($scope, ngDialog) {
			$scope.featured = [
				{ id: '#', title: 'U.S. World War II Army Enlistment Records' },
				{ id: '#', title: 'U.S. Civil War Soldiers, 1861-1865' },
				{ id: '#', title: 'WWII U.S. Navy Muster Rolls, 1938-1949' },
				{ id: '#', title: 'U.S. World War II Draft Registration Cards, 1942' },
				{ id: '#', title: 'World War I Draft Registration Cards' },
				{ id: '#', title: 'U.S. Marine Corp Muster Rolls, 1798-1958' },
			];

			$scope.openSubscribe = function() {
				ngDialog.open({
					template: 'views/modal_subscribe.html',
					controller: 'SubscribeCtrl',
					closeByDocument: false,
					closeByEscape: false
				});
			};

		}
	]
);
