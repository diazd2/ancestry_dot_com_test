# ancestry

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

Requires **Node.js**.

## Setup

Run `npm install & bower install` to install all npm and bower dependencies.


## Build & development

Run `grunt serve` to serve locally.